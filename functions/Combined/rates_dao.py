from collections import namedtuple
from mysqldb import MySql

DatabaseAccessInfo = namedtuple('DatabaseAccessInfo', ['host', 'port', 'database', 'username', 'password'])
MYSQL_DATE_FORMAT = '%Y-%m-%d'
MYSQL_TIME_FORMAT = '%H:%M:%S'

""""The query finds all records with matching entry and exit condition. Then it filters only rates wuth
both ENTRY and EXIT matching rows. Then it sorts based in price and choose the cheapest rate.
"""
QUERY_FIND_SPECIAL_RATE_BY_ENTRY_AND_EXIT = """
select r.`rate_id`, count(distinct c.`event`), max(r.`name`) as `name`, max(r.`price`) as `rate_price` , max(r.`type`) as `rate_type`
	from `conditions` as c
    left join `rates` as r
    on r.`rate_id` = c.`rate_id`
    where
        (
			(c.`event` = "ENTRY" and c.`day_of_week` = DAYOFWEEK({entry_date})  and {entry_time} between c.`from_time` and c.`to_time`)
        or
			(c.`event` = "EXIT" and c.`day_of_week` = DAYOFWEEK({exit_date})  and {exit_time} between c.`from_time` and c.`to_time`)
        ) and
        {stay_minutes} < r.`max_stay_minute`
    group by r.`rate_id`
	having count(distinct c.`event`) = 2
	order by `rate_price` desc
    limit 1
"""

QUERY_FIND_STANDARD_RATE_BY_STAY_DURATION = """
    select `rate_id` as `rate_id`, `name` as `name`, `price` as `rate_price` , `type` as `rate_type`
    from rates as r
    where
        `is_standard` = 1 and
        {stay_minutes} < r.`max_stay_minute`
    order by r.`max_stay_minute` asc
    limit 1
"""


class RateDao:

    def __init__(self, db_access_info: DatabaseAccessInfo) -> None:
        self._db_access_info = db_access_info
        super().__init__()

    def find_special_rate_by_entry_and_exit(self, entry_time, exit_time, stay_minutes):
        with MySql(**self._db_access_info._asdict()) as mysql_database:
            query = QUERY_FIND_SPECIAL_RATE_BY_ENTRY_AND_EXIT.format(
                entry_date=entry_time.strftime(MYSQL_DATE_FORMAT),
                entry_time=entry_time.strftime(MYSQL_TIME_FORMAT),
                exit_date=exit_time.strftime(MYSQL_DATE_FORMAT),
                exit_time=exit_time.strftime(MYSQL_TIME_FORMAT),
                stay_minutes=stay_minutes)
            values, names = mysql_database.execute_select_query(query)
        rate_list = [dict(zip(names, value)) for value in values]
        return rate_list[0] if rate_list else None

    def find_standard_rate_by_stay_duration(self, stay_minutes):
        with MySql(**self._db_access_info._asdict()) as mysql_database:
            query = QUERY_FIND_SPECIAL_RATE_BY_ENTRY_AND_EXIT.format(stay_minutes=stay_minutes)
            values, names = mysql_database.execute_select_query(query)
        rate_list = [dict(zip(names, value)) for value in values]
        return rate_list[0] if rate_list else None
