from collections import namedtuple
from enum import Enum
import collections
import json

ErrorBody = namedtuple('ErrorBody', ['type', 'message', 'requestId'])


class HttpStatus(Enum):
    SUCCESS = 200
    BAD_REQUEST = 400
    UNAUTHORIZED = 401
    FORBIDDEN = 403
    NOT_FOUND = 404
    INTERNAL_SERVER_ERROR = 500


class ApiHandlerError(Exception):
    def __init__(self, http_status: HttpStatus, message: str) -> None:
        self.http_status = http_status
        self.message = message


class _ProxyIntegrationResponse(collections.Mapping):
    def __init__(self, status_code, body, headers=None, base_64_encoded=None) -> None:
        self._items = dict()
        self._items['statusCode'] = status_code
        if body is not None:
            self._items['body'] = body if isinstance(body, str) else json.dumps(body)
        if headers:
            self._items['headers'] = headers  # optional.
        if base_64_encoded:
            self._items['isBase64Encoded'] = base_64_encoded  # optional. default is false

    def __getitem__(self, key):
        return self._items[self.__key_transform__(key)]

    def __iter__(self):
        return iter(self._items)

    def __len__(self):
        return len(self._items)

    def __key_transform__(self, key):
        return key


class ApiProxyIntegrationResponse(_ProxyIntegrationResponse):
    def __init__(self, status_code: HttpStatus, body: dict, headers: dict=None) -> None:
        self._headers = headers if headers else dict()
        self._add_default_headers()
        super().__init__(status_code=status_code, body=body, headers=self._headers, base_64_encoded=False)

    def _add_default_headers(self):
        self._headers['Content-Type'] = 'application/json'
        self._headers['Access-Control-Allow-Headers'] = 'Content-Type,X-Amz-Date,Authorization,X-Api-Key'
        self._headers['Access-Control-Allow-Methods'] = '*'
        self._headers['Access-Control-Allow-Origin'] = '*'

    @classmethod
    def make_success_response(cls, body) -> 'ApiProxyIntegrationResponse':
        return cls(status_code=HttpStatus.SUCCESS.value, body=body)

    @classmethod
    def make_failure_response(cls, error: ApiHandlerError, request_id: str) -> 'ApiProxyIntegrationResponse':
        body = ErrorBody(type=error.http_status.name, message=error.message, requestId=request_id)
        return cls(status_code=error.http_status.value, body=body._asdict())
