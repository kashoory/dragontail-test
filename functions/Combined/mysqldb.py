import mysql.connector
from mysql.connector import errorcode


MYSQL_CONNECTION_TIMEOUT = 20   # seconds


class Error(Exception):
    """Exception that is base class for all other error exceptions"""
    def __init__(self, msg=None):
        super(Error, self).__init__()
        self.msg = msg

    def __str__(self):
        return self.msg


class HostError(Error):
    pass


class AuthenticationError(Error):
    pass


class SchemaError(Error):
    pass


class DataError(SchemaError):
    pass


class InvalidQueryError(Error):
    pass


class SelectQueryError(Error):
    pass


class ManipulatorQueryError(Error):
    pass


class MySql:
    def __init__(self, host, port, username, password, database):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.database = database

    def __enter__(self):
        # get connection
        try:
            self.connection = mysql.connector.connect(host=self.host, port=self.port, database=self.database,
                                                      user=self.username, password=self.password,
                                                      connection_timeout=MYSQL_CONNECTION_TIMEOUT)
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                raise AuthenticationError("Username or password is wrong")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                raise SchemaError("Database does not exist")
            elif err.errno == errorcode.CR_CONN_HOST_ERROR:
                raise HostError("Host error or timeout")
#            elif err.errno == errorcode.ER_DBACCESS_DENIED_ERROR:
#                raise AuthenticationError("Access denied to database!")
            else:
                raise Error(str(err))

        else:
            return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.connection.close()

    def execute_select_query(self, query):
        try:
            # TODO any timeout applies here?!
            cursor = self.connection.cursor()
            cursor.execute(query)
            return cursor.fetchall(), cursor.column_names
        except mysql.connector.Error as err:
            raise SelectQueryError("Unable to run select query: {}\nError: {}".format(query, str(err)))

    def execute_manipulator_query(self, query):
        try:
            # TODO any timeout applies here?!
            cursor = self.connection.cursor()
            cursor.execute(query)
            self.connection.commit()
            return
        except mysql.connector.Error as err:
            raise SelectQueryError("Unable to run manipulator query: {}\nError: {}".format(query, str(err)))

    def execute_many_manipulator_query(self, query, values):
        try:
            # TODO any timeout applies here?!
            cursor = self.connection.cursor()
            cursor.executemany(query, values)
            self.connection.commit()
            return
        except mysql.connector.Error as err:
            raise SelectQueryError("Unable to run manipulator query: {}\nError: {}".format(query, str(err)))
