from apigateway.event import ApiProxyIntegrationEvent
from apigateway.response import ApiHandlerError, HttpStatus, ApiProxyIntegrationResponse
from constants import DRAGON_DB
from rates_dao import RateDao, DatabaseAccessInfo
import dateutil
import math


def handler(event, context):
    request_id = 'Unknown'
    try:
        api_event = ApiProxyIntegrationEvent(event)
        request_id = api_event.request_id

        result = process_calculate_api_request(api_event)
    except Exception as e:
        error = e if isinstance(e, ApiHandlerError) else \
            ApiHandlerError(HttpStatus.INTERNAL_SERVER_ERROR, 'An unexpected error occurred.')
        return dict(ApiProxyIntegrationResponse.make_failure_response(error, request_id))
    else:
        return dict(ApiProxyIntegrationResponse.make_success_response(body=result))


def process_calculate_api_request(api_event: ApiProxyIntegrationEvent) -> dict:
    body = api_event.body
    if 'EntryDate' not in body or 'ExitDate' not in body:
        raise ApiHandlerError(HttpStatus.BAD_REQUEST, 'Invalid request body. Missing EntryDate or ExitDate.')
    try:
        entry_datetime = dateutil.parser.parse(body['EntryDate'])
        exit_datetime = dateutil.parser.parse(body['ExitDate'])
    except ValueError:
        raise ApiHandlerError(HttpStatus.BAD_REQUEST, 'Invalid request body. Invalid date-time format.')

    db_access_info = DatabaseAccessInfo(host=DRAGON_DB['host'], port=DRAGON_DB['port'],
                                        database=DRAGON_DB['database'],
                                        username=DRAGON_DB['user'], password=DRAGON_DB['password'])
    rates_dao = RateDao(db_access_info)
    stay_minute = math.ceil((exit_datetime - entry_datetime).seconds / 60)
    matching_special_rate = rates_dao.find_special_rate_by_entry_and_exit(entry_datetime, exit_datetime, stay_minute)
    if matching_special_rate:
        return {'Rate': matching_special_rate['rate_price'], 'RateName': matching_special_rate['rate_name']}

    matching_standard_rate = rates_dao.find_standard_rate_by_stay_duration(stay_minute)

    if matching_standard_rate['rate_type'] == 'FLAT':
        rate = matching_special_rate['rate_price']
    else:
        # we assume that price is calculated based on number of hours rounded up
        stay_hour = math.ceil((exit_datetime - entry_datetime).seconds / 3600)
        rate = matching_special_rate['rate_price'] * stay_hour
    return {'Rate': rate, 'RateName': matching_special_rate['rate_name']}
