CREATE TABLE `rates` (
	`rate_id` smallint(4) unsigned,
	`type` enum('FLAT','HOURLY') NOT NULL,
	`is_standard` tinyint(1) NOT NULL,
	`name` varchar(45) NOT NULL,
	`price` smallint(4) unsigned NOT NULL,
	`max_stay_minute` smallint(5) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `conditions` (
	`rate_id` smallint(4) unsigned,
	`day_of_week` smallint(4) unsigned,

	`event` enum('ENTRY','EXIT') NOT NULL,
	`from_time` time NOT NULL,
	`to_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

