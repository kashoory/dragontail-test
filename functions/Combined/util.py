from enum import Enum


def make_namedtuple_from_tuples(namedtuple_type, field_names, values):
    return namedtuple_type(**dict(zip(field_names, values)))


def enums_to_values(enum_elements):
    assert type(enum_elements) in (list, set, tuple, Enum)
    if isinstance(enum_elements, Enum):
        return enum_elements.value
    values = map(lambda x: x.value, enum_elements)
    return type(enum_elements)(values)
