
call pip install -r .\functions\Combined\requirements.txt -t .\functions\Combined\
call aws cloudformation package --template-file template.yaml --s3-bucket my_upload_bucket --output-template-file template-packaged.yaml
call aws cloudformation deploy --template-file template-packaged.yaml --stack-name DragonParkStack --capabilities CAPABILITY_NAMED_IAM --parameter-overrides %*