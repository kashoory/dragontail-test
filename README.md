### The project

This project implements part of Dragon's Park test.


### IDE

PyCharm IDE is used for this project.

### API implementation

The API is implemented via AWS API Gateway and AWS Lambda function. The file template.yaml is a CloudFormation 
template based on SAM (Serverless Application Model) that defines a stack including an API Gateway plus two Lambda
functions and relevant connections. Each Lambda function implement one API call.
swagger.yaml is the API specification that define the API paths and methods plus some API Gateway parameters.

### Database

For database, MySql is used to store rates information. table-create.sql includes table creation statements.
Table "rates" defines all rates and their specification like whether they are standard rate or special (like nightly, 
early bird, etc) as well as the type of rate applied (whether it is flat or hourly).
Please note that we have more than one standard rate. For, example 0-1hr is one rate and 1-2 hour is a different rate.
The field "max_stay_minute" is used to restrict each rate to specific time duration. For example Night Rate cannot 
span more than 12 hours. Also, this field distinguish different standard rate from each other.
Table "conditions" specifies entry and exit times. For each rate, we define all allowed entry and exit times per days. 


### Deployment

The batch file deploy.bat install required python packages and deploy the AWS stack. We removed AWS account ID from 
swagger.yaml. You should insert your own account ID (replace ACCOUNT_NUMBER). Also you mat need to set change the 
origin from ap-southeast-2 to origin of choice. 

### Note

Due to lack of time I could only implement one API CalculateParkingRate. Also I was unable to test the 
solutions and implementation. Almost all parts are not tested.
 
