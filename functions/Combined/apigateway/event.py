
class ApiProxyIntegrationEvent:
    def __init__(self, event: dict) -> None:
        self._event = event
        self._validate_event()
        self._request_id = self._event['requestContext']['requestId']
        self._resource = self._event['resource']
        self._path = self._event['path']
        self._http_method = self._event['httpMethod']
        self._path_params = self._event['pathParameters']
        self._query_params = self._event['queryStringParameters']
        self._headers = self._event['headers']
        self._body = self._event['body']

    def _validate_event(self) -> None:
        # We just check a couple of fields here
        if not {'requestContext', 'resource', 'httpMethod', 'path'}.issubset(self._event.keys()):
            raise ValueError('Invalid API event.')

    @property
    def resource(self) -> str:
        return self._resource

    @property
    def path(self) -> str:
        return self._path

    @property
    def http_method(self) -> str:
        return self._http_method

    @property
    def path_params(self) -> dict:
        return self._path_params if self._path_params is not None else {}

    @property
    def query_params(self) -> dict:
        return self._query_params if self._query_params is not None else {}

    @property
    def headers(self) -> dict:
        return self._headers if self._headers is not None else {}

    @property
    def body(self) -> dict:
        return self._body

    @property
    def request_id(self) -> str:
        return self._request_id
