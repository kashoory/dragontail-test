from apigateway.response import ApiProxyIntegrationResponse


def handler(event, context):
    return dict(ApiProxyIntegrationResponse.make_success_response(body={'message': 'not implemented'}))

